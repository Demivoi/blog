---
title: Dag 13
date: 2017-10-11
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Op woensdag 11 oktober hebben we weer een studio dag gehad. Op deze dag hebben we taken verdeeld voor het project en gekeken wie, wat in de vakantie zou gaan doen.
Ik had als taak gekregen om verder te werken aan het prototype en de app. Ik vond dit een leuke taak omdat het creatief was
en grafisch, wat ik al was gewend van mijn vorige opleiding. Rosa zou de posters maken en deze heb ik kunnen printen om mijn werk voor de expositie.
In de vakantie heb ik ook nog een dag samen gewerkt met Lotte aan het prototype en hebben we alles samen nog een keer doorgenomen.