---
title: Kwartaal reflectie
date: 2018-05-31
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Het kwartaal zit er weer op. Ik heb veel geleerd maar ook tegen een aantal dingen aangelopen die ik samen met mijn projectgroep heb opgelost.
Ik vond het een leuke periode omdat we weer met dezelfde groep aan de slag gingen en we dus al aan elkaar gewend waren.
De tijd leek te vliegen, en we hebben veel gemaakt in een korte tijd. 
Gelukkig hebben we wel alles afgekregen voor de expo, en hebben we samen iets moois neergezet waar we trots op zijn.
Ik vond de samenwerking het beste gaan deze periode. We hebben geen ruzies gehad. Af en toe wel discussies
maar dat hoort er natuurlijk gewoon bij. Wat minder ging deze periode was misschien de tijdsplanning.
We hadden op het laatste moment erg veel te doen waardoor we net niet in de knel kwamen met de tijd.
Gelukkig is alles wel goed gekomen, en hebben we het jaar samen goed afgesloten.
