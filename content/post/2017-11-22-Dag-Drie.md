---
title: Dag 3
date: 2017-11-22
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Toen we de dag begonnen hebben we eerst weer de planning bekeken. We hadden de deliverables van de week al af dus we besloten te gaan brainstormen voor een idee. Ik heb alles genotuleerd, we hebben een mindmap gemaakt en concepten opgeschreven. Nadat we dit hadden gedaan hadden we even pauze en daarna gingen we verder met de user-journey. Dit was nieuw voor mij, maar Lorenzo had hier al ervaring mee dus daar kon ik van leren.
Ik heb de user-journey genotuleerd, en we hebben er met zijn alle voor gezeten. Het moet nu alleen nog digitaal  gemaakt worden zodat we het kunnen inleveren.
Hierna was de dag al zo ver als voorbij en heb ik mijn blogpost geschreven en de dag afgesloten. 