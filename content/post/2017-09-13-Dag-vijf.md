---
title: Dag 5
date: 2017-09-13
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Woensdag hebben we een korte presentatie gehad over ons prototype. We hebben met medestudenten rond de tafel gestaan en iedereen heeft zijn prototype laten zien. Veel mensen hadden hetzelfde idee als wij. Er waren veel kartonnen Iphone’s gemaakt, met daarin sheets van apps. Wat ik goed vond van een ander groepje is dat ze een lange sheet hadden gemaakt die je kon doorschuiven in de kartonnen Iphone. Op deze manier zag je duidelijker hoe je bij een bepaald scherm kwam en ook weer makkelijk terug. Ook was er een groepje die vaste buttons onderaan hun prototype Iphone hadden gemaakt die bij elk scherm van de app te zien was, op deze manier hoef je die niet overal te tekenen, dat vond ik slim. We kregen goede feedback, ze vonden het alleen jammer dat je bij ons prototype niet goed kon zien hoe je terug kon in de app, dit is een punt voor de volgende keer.

Na de presentaties gingen we de spelanalyse doen en gingen we met zijn vieren de stad in. We hebben een vraag beantwoord van onze app, en zijn we naar de Erasmus brug gelopen om te kijken hoe de afstand eigenlijk is, en of het wel leuk is om met mensen die je niet kent zulke afstanden te lopen. De resultaten hiervan hebben we genoteerd in onze spelanalyse.