---
title: Zelf reflectie
date: 2018-03-12
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Ik vond de samenwerking deze periode erg goed gaan. Ik kende mijn groep al redelijk goed dus we waren al goed op elkaar ingespeeld.
We hebben eigenlijk geen ruzies of moeilijkheden gehad, waar ik heel blij mee ben.
Ik heb veel geleerd op het gebied van de competenties en ik ben beter geworden in het schrijven van STARRT'S.
Ik vond het leuk om te werken aan de opdracht en ik heb meer taken op me weten te nemen. Ik heb ook nieuwe leerdoelen opgesteld in mijn
dit ben ik profiel, en gereflecteerd op mijn eerdere leerdoelen. 
Ik heb zin om verder te werken aan de opdracht voor een nieuwe doelgroep. 
Vorige periode wilde ik ervoor zorgen dat ik meer feedback zou gaan vragen, en hier heb ik me aan gehouden. Zeker ook omdat dit nodig is om competenties te behalen.
