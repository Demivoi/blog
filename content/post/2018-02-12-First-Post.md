---
title: Dag 1
date: 2018-02-12
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Op maandag zijn we begonnen met een presentatie over de groepen voor deze design challenge. De teams waren ingedeeld voor ons en ik zit deze periode met lorenzo, Remco en Dominique in een groep. Toen we samen zaten hebben we samen de opdracht besproken en zijn we alvast begonnen met een plan van aanpak, omdat we deze moeten gaan presenteren op woensdag. Ik heb alle verzamelde informatie in een poster gezet zodat we dit duidelijk kunnen laten zien aan de studiobegeleiders. 