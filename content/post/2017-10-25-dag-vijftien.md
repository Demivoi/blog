---
title: Dag 15
date: 2017-10-25
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

De dag van de expo!
De dag begon een beetje druk omdat een van ons teamgenoten iets te laat was door het OV.
Gelukkig hebben we dit goed op weten te lossen en hebben we ons tafel goed kunnen indelen. 
Toen de expo begon hebben we de pitch goed doorgenomen en ik heb de pitch ook een paar keer gehouden 
voor voorbijgangers. Het was fijn om feedback te krijgen en om te zien dat andere studenten ook enthousiast waren
over onze game. Het was een leuke afsluiting van de eerste design challenge.
Als ik terug kijk op deze periode ben ik erg tevreden. Ik ben er trots op om te zien wat
we in deze korte tijd met de groep hebben weten neer te zetten. Ik heb ook veel geleerd van deze design challenge.
Het was ook fijn om in een groep te werken omdat je op deze manier ook van elkaar leert en je leert je op andere mensen
af te stemmen.

Ik heb zin in de volgende periode!