---
title: Dag 12
date: 2018-05-30
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Gister was de dag dat we gingen testen, dus vandaag was het tijd voor een testrapport.
Ik heb alles in een verslag gezet en verwerkt met foto's. Nadat ik hier mee klaar was 
zijn we verder gegaan met wat voorbereidingen voor de expo. We hebben kleine foodtrucks gevouwen, en nog 
het een en ander besproken. 