---
title: Dag 8/9
date: 2018-03-20
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Op maandag was er een studio vrije dag. Ik ben wel naar de studio geweest om mijn ontwerpcriteria te laten valideren. Ik was al eerder geweest voor feedback, maar het is nog niet gevalideerd. Ik ga mijn feedback toepassen. 
Op dinsdag hadden we SC les en hebben we feedback gegeven aan elkaar over ons gemaakte STARRS, na de SC zijn we verder gegaan aan ons project. We hebben de gemaakte lowfid prototypes en de gemaakte storyboards aan elkaar gepresenteerd en dit proberen samen te voegen. We hebben besloten om op de woensdag feedback op ons gemaakte producten te vragen aan Bob. 
