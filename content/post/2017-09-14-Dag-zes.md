---
title: Dag 6
date: 2017-09-14
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Op 14 september heb ik cursus Sketch gehad in de ochtend. Ik denk dat ik met deze cursussen wel informatie kan opdoen die nuttig is voor ons team. Het is een handig programma om duidelijk prototypes te maken, dus wellicht kunnen we het later gebruiken voor een prototype.
Na de sketch les hadden we nog tijd over om individueel nog alles klaar te maken voor het inleveren op vrijdag. Ik heb al mijn bestanden gecheckt en ik heb alle bestanden die we in team verband hebben gemaakt samen gevoegd en ingeleverd.