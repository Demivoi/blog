---
title: Dag twee
date: 2017-11-20
author: "Demi van Oirschot"
---
De dag begon met een soort stand-up. We hebben de het team de planning bekeken en we gingen aan de slag. 
We zijn samen begonnen met het maken van een merk-analyse. Ikzelf heb een merkanalyse gemaakt van concurenten van de opdracht gever. 
Het was best interessant want ik had nog nooit eerder zoiets gemaakt. 
Toen de merk analyses waren gemaakt hebben we nog een beetje gebrainstormd voor een concept, 
dit was nog best lastig omdat we pas aan het begin van de opdracht staan. Na de brainstormsessie hadden we les van de studiecoach. 
We zijn met de groep apart gaan zitten en we hebben teamafspraken opgesteld en een samenwerkingscontract gemaakt. 
Ook moesten we de SWOT-analyse verder uitwerken en dit hebben we meteen ingeleverd.