---
title: New start
date: 2017-11-15
author: "Demi van Oirschot"
---
Dit was de eerste studiodag van Design Challenge 2. 
We begonnen de dag met het maken van de nieuwe teams. 
Dit was wel even een gedoe maar uiteindelijk is iedereen eruit gekomen. 
Het is toch wel een belangrijk deel van de opdracht met wie je samen werkt. 
We hebben uiteindelijk het team dat bestaat uit Mike, Remco, Puck, Lorenzo en ik. 
Toen we het team hadden gevormd begonnen we met elkaar leren kennen en kijken wat ons te wachten zou staan. 
We hebben samen de briefin bekeken en een begin gemaakt aan de planning. 
We besloten om de planning te maken in Trello. Ik had hier nog nooit mee gewerkt maar het is een prettige manier van werken. 
Het is eigenlijk een scrumbord maar dan online waar iedereen in kan kijken. We hebben ook een begin gemaakt aan de swot-analyse. 
Dit was nog wel lastig omdat we elkaar natuurlijk nog niet goed kennen. Daarna heb ik mijn blog post geschreven en toen zat de dag er al weer op.
