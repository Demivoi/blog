---
title: Dag 12
date: 2017-10-09
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Op 9 oktober kregen we een briefing voor de laatste iteratie. Het kwam erop neer dat we nog 2 weken hadden om ons concept goed te maken, een heel nieuw prototype te maken, een design en een plan voor onze expositie. Ik wist niet dat we het concept zouden gaan exposeren dus dit kwam best wel als een verassing. We moesten ook nog een goede pitch bedenken voor ons concept, en gaan kijken hoe we ons concept willen gaan verkopen in maar 1 minuut. 
We hebben rond de tafel gezeten en het concept goed op een rijtje gezet zodat we het begrepen. Ook hebben we feedback gevraagd aan bob, en hebben we nog eens goed naar de doelgroep gekeken, eermiddel van een soort creatieve techniek waarbij we kenmerken opschreven van de doelgroep. Nadat we dit hadden gedaan zijn Lotte en ik gaan kijken naar een naam voor het spel en een logo.