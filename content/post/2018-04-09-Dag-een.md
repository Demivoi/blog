---
title: Dag 1
date: 2018-04-09
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Vandaag was de kick-off van het nieuwe project. Het project wordt voorgezet met dezelfde groep als tijdens design challenge 4. Dit keer is de doelgroep veranderd en mogen we ons focussen op de doelgroep, 18 tot 25 jarige in Rotterdam Zuid. Na de kick-off en een korte uitleg zijn we aan de slag gegaan met een nieuw plan van aanpak en met een planning. 