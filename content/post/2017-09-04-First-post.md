---
title: First post!
date: 2017-09-04
author: "Demi van Oirschot"
---

Vandaag begonnen we met het maken van een scrumbord. Hierop hebben we alle taken geschreven en nagedacht over wie wat doet. 
Ook heb ik als teamleider geluisterd naar uitleg en onze meningen besproken over de werkplek. We hebben regels opgesteld en besproken.
Ook hebben we de onderzoeksvragen opgesteld en deze onderverdeeld. Ik heb spellen onderzocht die je laten samenwerken in een teamverband.
Daarnaast hebben we ook een enquete opgesteld en een doelgroep bepaald.
Onze doelgroep is studenten van de opleiding fysiotherapie 1e jaars.

We kregen ook een opdracht om als groep een poster te maken over onze afspraken en kwaliteiten, 
achteraf hebben we van iedere groep de poster bekeken. 
Ik vond het leuk en interessant om te zien dat iedereen andere regels heeft en dat iedereen er anders instaat.
