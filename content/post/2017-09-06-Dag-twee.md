---
title: Dag 2
date: 2017-09-06
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Op woensdag ben ik de dag begonnen met een stand-up van de team captains. We hebben besproken hoe het er bij iedereen aan toe gaat en waar iedereen ongeveer is met de design challenge. Ik vind de samenwerking tot nu toe goed gaan, we hebben goede afspraken gemaakt en iedereen probeert zich hier goed aan te houden.

In de studio ben ik begonnen met het visualiseren van mijn onderzoek, ik had alleen nog maar tekst getypt en nu heb ik er een schema bij gemaakt zodat het er overzichtelijker uitziet.
Het moodboard voor onze doelgroep is ook afgemaakt. We hadden goed afgesproken dat we verder gingen met het individuele onderzoek.

Na de pauze hebben we de 1e workshop gevolgd over het maken van een blog.