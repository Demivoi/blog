---
title: Dag 14
date: 2017-10-23
author: "Demi van Oirschot"
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---

Dit was de laatste dag voor de expo!
We gingen met de hele groep nog een laatste keer rond de tafel om te kijken of iedereen
af had wat nog gedaan moest worden voor de expo. Lotte en ik gingen samen nog een keer door de app
heen om te kijken of alles klopte wat we hadden gemaakt, we gingen alle bestanden nog even bij elkaar zetten
voor de oplevering en we gingen met zijn alle bespreken hoe we het wilde gaan aanpakken tijdens de expo.
We hadden posters, een gif voor op de laptop en natuurlijk het prototype dat ondertussen helemaal werkend was gemaakt.
We waren er klaar voor :)